import csv
import json
from pathlib import Path

import requests


def get_location_from_ipinfo_io(ip):
    resp = requests.get(f'https://ipinfo.io/{ip}')
    content = resp.content.decode('utf-8')
    json_dict = json.loads(content)
    if json_dict.get('bogon') is True:
        return None
    return json_dict

    # print(html)
    # idx = html.find('<i class="flag flag-gb"></i>')
    # if idx < 0:
    #     return None
    # html = html[idx+len('<i class="flag flag-gb"></i>'):]
    # html = html.strip()
    # loc = html.split()[0]
    # return loc


def test_log_analysis():
    with open(Path('~').expanduser()/'Downloads'/'downloaded-logs-20241214-212542.csv') as fd:
        r = csv.DictReader(fd)
        with open(Path('~').expanduser()/'Downloads'/'downloaded-logs-20241214-212542-ipinfo.csv', 'w', encoding='utf-8') as wfd:
            w = None # csv.DictWriter(wfd)
            for l in r:
                if l['protoPayload.ip']:
                    print('ip=', l['protoPayload.ip'])
                    info = get_location_from_ipinfo_io(l['protoPayload.ip'])
                    if info:
                        if not w:
                            w = csv.DictWriter(wfd, list(info.keys()))
                            w.writeheader()
                        w.writerow(info)
                if l['protoPayload.requestMetadata.callerIp']:
                    print('req-meta-caller-ip=', l['protoPayload.requestMetadata.callerIp'])
                    info = get_location_from_ipinfo_io(l['protoPayload.requestMetadata.callerIp'])
                    if info:
                        if not w:
                            w = csv.DictWriter(wfd, list(info.keys()))
                            w.writeheader()
                        w.writerow(info)

