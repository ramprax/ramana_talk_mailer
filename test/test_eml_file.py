import email
import re

import unicodedata
from google.appengine.api import mail

def slugify_custom(value):
    """
    Normalizes string, converts to lowercase, removes non-alpha characters,
    and converts spaces to hyphens.
    Copied from django
    """
    value = unicodedata.normalize('NFKD', value)  # .encode('ascii', 'ignore')
    value = re.sub(r'[^\w\s-]', '', value).strip().lower()
    return re.sub(r'[-\s]+', '-', value)


def main():
    # ramprakash-radhakrishnan-ramprakashradhakrishnangm.eml
    # anja_email_bsrm.eml
    with open(r'C:\Users\rampr\Downloads\ramprakash-radhakrishnan-ramprakashradhakrishnangm.eml') as fd:
        raw_mail_data = fd.read()
        original_email = email.message_from_string(raw_mail_data)
        # print original_email
        # print dir(original_email)
        iem = mail.InboundEmailMessage(raw_mail_data)
        # print(iem)
        # print(dir(iem))
        print('raw to:', original_email['to'])
        print('raw from:', original_email['from'])
        print('raw sender:', original_email['sender'])

        print('iem to:', iem.to)
        print('iem sender:', iem.sender)
        print('iem subject:', iem.subject)

        mail_filename = slugify_custom(iem.sender)[:25] + '_' + slugify_custom(iem.subject)[:25]
        print(mail_filename)

        for content_type, b in iem.bodies(content_type='text/plain'):
            print(content_type)
            # print b
            text_body = b.decode()
            quoted_mail_stripped = '\n'.join([line for line in text_body.split('\n') if not line.startswith('> ')])
            print(quoted_mail_stripped)

        # print iem.body.decode()
    pass


if __name__ == '__main__':
    main()

