from time import sleep
from datetime import date
from google.appengine.ext import ndb
from rr_daily.naan_yaar import content
from rr_daily.naan_yaar.content import PostContentSource, PostInfo

__author__ = 'ramprax'
import unittest

from rr_daily.common.content.models import POST_MAILING_STATUS_RECEIVED

TEST_BOOK_AUTHOR_1 = 'Ramana'
TEST_BOOK_NAME_1 = 'Ramana test book 1'
TEST_BOOK_NAME_2 = 'Ramana test book 2'
TEST_BOOK_NAME_3 = 'Ramana test book 3'
TEST_POST_NAME_1 = 'Excerpt from Ramana test book 1'
TEST_POST_NAME_2 = 'Excerpt from Ramana test book 2'
TEST_POST_NAME_3 = 'Excerpt from Ramana test book 3'


class TodayPostChooserTest(unittest.TestCase):

    def setUp(self):
        self.book1 = PostContentSource()
        self.book1.num_available_posts = 10
        self.book1.posted_count = 0
        self.book1.author = TEST_BOOK_AUTHOR_1
        self.book1.name = TEST_BOOK_NAME_1
        self.book1.post_name = TEST_POST_NAME_1
        self.book1.is_ready_to_mail = True
        self.book1.put()

        self.book2 = PostContentSource()
        self.book2.num_available_posts = 1
        self.book2.posted_count = 1
        self.book2.author = TEST_BOOK_AUTHOR_1
        self.book2.name = TEST_BOOK_NAME_2
        self.book2.post_name = TEST_POST_NAME_2
        self.book2.is_ready_to_mail = True
        self.book2.put()

        self.b2pi1 = PostInfo(
            post_name=self.book2.post_name,
            index=1,
            posted_count=1,
            last_posted=date(2015, 4, 14),
            current_posting_status=POST_MAILING_STATUS_RECEIVED,
            posted_dates=[date(2015, 4, 14)]
        )
        self.b2pi1.put()

        self.book3 = PostContentSource()
        self.book3.num_available_posts = 2
        self.book3.posted_count = 3
        self.book3.author = TEST_BOOK_AUTHOR_1
        self.book3.name = TEST_BOOK_NAME_3
        self.book3.post_name = TEST_POST_NAME_3
        self.book3.is_ready_to_mail = True
        self.book3.put()
        self.b3pi1 = PostInfo(
            post_name=self.book3.post_name,
            index=1,
            posted_count=1,
            last_posted=date(2014, 9, 1),
            current_posting_status=POST_MAILING_STATUS_RECEIVED,
            posted_dates=[date(2014, 9, 1)]
        )
        self.b3pi1.put()
        self.b3pi2 = PostInfo(
            post_name=self.book3.post_name,
            index=2,
            posted_count=2,
            last_posted=date(2015, 4, 15),
            current_posting_status=POST_MAILING_STATUS_RECEIVED,
            posted_dates=[date(2014, 12, 29), date(2015, 4, 15)]
        )
        self.b3pi2.put()

        sleep(1)

    def tearDown(self):
        for pn in (TEST_POST_NAME_1, TEST_POST_NAME_2, TEST_POST_NAME_3):
            ndb.delete_multi(
                PostContentSource.query(
                    PostContentSource.post_name == pn
                ).fetch(keys_only=True)
            )
            ndb.delete_multi(
                PostInfo.query(
                    PostInfo.post_name == pn
                ).fetch(keys_only=True)
            )


    def test_00_ChoiceMinZero(self):
            pi = content.choose_today_post()
            assert pi.post_name == self.book1.post_name
            assert 1 <= pi.index <= self.book1.num_available_posts
            assert pi.posted_count == 0

    def test_01_ChoiceMinNonZero(self):
        self.book1.key.delete()
        self.book1 = None
        sleep(1)

        pi = content.choose_today_post()
        assert pi.post_name == self.b2pi1.post_name
        print('pi.index', pi.index, '| b2pi1 index', self.b2pi1.index)
        assert pi.index == self.b2pi1.index
        assert pi.posted_count == self.b2pi1.posted_count

