import os
import sys
import threading

from time import sleep

__author__ = 'ramprax'

import unittest
import queue

def _complain_ifclosed(closed):
    if closed:
        raise ValueError("I/O operation on closed file")

class TestStream:
    def __init__(self):
        self.closed = False
        self.out = queue.Queue()

    def write(self, x):
        _complain_ifclosed(self.closed)
        self.out.put(x)

    def flush(self):
        pass

    def close(self):
        self.closed = True

    def get_values(self):
        if self.out.empty():
            _complain_ifclosed(self.closed)
        while not self.out.empty():
            yield self.out.get()


def runtests():
    if not os.environ['SERVER_SOFTWARE'].startswith('Development'):
        return 'Not in Dev mode'
    suite = unittest.loader.TestLoader().discover('test')
    my_output_stream = TestStream()

    def thread_run():
        result = unittest.TextTestRunner(stream=my_output_stream, verbosity=2).run(suite)
        sleep(1)
        for x in range(10):
            my_output_stream.write("{0} {1}\n".format(x, '.'*50))
            sleep(1)
        my_output_stream.close()

    def result_generator():
        try:
            while True:
                for x in my_output_stream.get_values():
                    print(x, file=sys.stderr, end=None)
                    yield x
        except:
            pass

    res_gen = result_generator()
    threading.Thread(target=thread_run).start()

    return res_gen
