__author__ = 'ramprax'

from google.cloud import ndb

class EmailDetail(ndb.Model):
    email = ndb.StringProperty()
    name = ndb.StringProperty()
    is_sender = ndb.BooleanProperty()
    is_receiver = ndb.BooleanProperty()
    is_admin = ndb.BooleanProperty()
    is_test = ndb.BooleanProperty()

    @classmethod
    def get_by_name(cls, name):
        return cls.query().filter(cls.name == name).get()

