__author__ = 'ramprax'

from rr_daily.common.appconfig.models import AppConfig
from rr_daily.common.gae_helper import wrap_with_ndb_client_context


@wrap_with_ndb_client_context
def get_app_config():
    return AppConfig.get_app_config()

