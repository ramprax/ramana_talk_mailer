__author__ = 'ramprax'

from google.cloud import ndb


class AppConfig(ndb.Model):
    app_secret_key = ndb.StringProperty(default='CHANGE ME')

    @classmethod
    def get_app_config(cls):
        cfg = AppConfig.query().get()
        if not cfg:
            cfg = AppConfig()
            cfg.put()
        return cfg
