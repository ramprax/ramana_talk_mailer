__author__ = 'ramprax'

import logging

from google.appengine.api.users import get_current_user
from google.cloud import ndb

from rr_daily.common.content.posts import get_post_content, get_post_content_source
from rr_daily.common.content.models import Correction, CORRECTION_STATUS_DRAFT, CORRECTION_STATUS_FINAL


#@db.transactional
def upload_new_correction(daily_post_name, index, new_text, new_html):
    the_content = get_post_content(daily_post_name, index)
    src = the_content.content_source()
    if not the_content:
        raise Exception('No {0} with index {1} found'.format(daily_post_name, index))
    correction = get_saved_draft_correction(daily_post_name, index)
    if correction:
        raise DraftAlreadyPresentException(daily_post_name, index)

    if not the_content.version:
        the_content.version = 1
        the_content.put()

    correction = Correction(
        src_kind=daily_post_name,
        src_index=index,
        status=CORRECTION_STATUS_DRAFT,
        proposed_text=new_text,
        proposed_html=new_html,
        content_version=the_content.version,
        corrected_by=get_current_user(),
    )
    correction.put()
    return correction.key


#@db.transactional
def update_saved_correction(daily_post_name, index, new_text, new_html):
    the_content = get_post_content(daily_post_name, index)
    src = the_content.content_source()
    if not the_content:
        raise Exception('No {0} with index {1} found'.format(daily_post_name, index))
    correction = get_saved_draft_correction(daily_post_name, index)
    if not correction:
        raise NoDraftFoundException(daily_post_name, index)
    correction.proposed_text = new_text
    correction.proposed_html = new_html
    correction.put()
    return correction.key


#@db.transactional
def get_saved_draft_correction(daily_post_name, index, check_version=True):
    the_content = get_post_content(daily_post_name, index)
    src = the_content.content_source()
    if not the_content:
        raise Exception('No {0} with index {1} found'.format(daily_post_name, index))
    q = Correction.query()
    q.filter(Correction.src_kind == daily_post_name)
    q.filter(Correction.src_index == index)
    q.filter(Correction.status == CORRECTION_STATUS_DRAFT)
    count = q.count(limit=2)
    logging.info("Number of corrections found = {0}".format(count))
    if count > 1:
        raise TooManyDraftsFoundException(daily_post_name, index)
    correction = q.get()
    if check_version:
        if correction is not None and correction.content_version != the_content.version:
            raise CorrectionVersionMismatchException(
                daily_post_name,
                index,
                correction.content_version,
                the_content.version
            )
    return correction



#@db.transactional
def discard_correction(correction_key):
    ck = ndb.Key(correction_key)
    if ck.kind() == 'Correction':
        Correction.get_by_id(ck.id()).delete()
    else:
        raise Exception('Invalid key')


#@db.transactional
def confirm_correction(correction_key):
    correction = None
    ck = ndb.Key(correction_key)
    if ck.kind() == 'Correction':
        correction = Correction.get_by_id(ck.id())
    else:
        raise Exception('Invalid key')
    if correction is None:
        raise Exception('Invalid key')
    if correction.status != CORRECTION_STATUS_DRAFT:
        raise CorrectionException(correction.src_kind, correction.src_index, 'Correction not in DRAFT status')

    # Update content of content table with the new correction
    content_src = get_post_content_source(correction.src_kind)
    the_content = content_src.load_correction(
        correction.src_index,
        correction.proposed_title,
        correction.proposed_text,
        correction.proposed_html,
        correction.content_version
    )
    if the_content is not None:
        # Mark correction as 'FINAL'/remove correction record
        correction.status = CORRECTION_STATUS_FINAL
        correction.put()


class CorrectionException(Exception):
    def __init__(self, src_kind, src_index, msg):
        super(CorrectionException, self).__init__(msg)
        self.src_kind = src_kind
        self.src_index = src_index


class DraftAlreadyPresentException(CorrectionException):
    def __init__(self, src_kind, src_index):
        super(DraftAlreadyPresentException, self).__init__(
            src_kind,
            src_index,
            'There is already a DRAFT correction for {0} {1}'.format(src_kind, src_index)
        )


class NoDraftFoundException(CorrectionException):
    def __init__(self, src_kind, src_index):
        super(NoDraftFoundException, self).__init__(
            src_kind,
            src_index,
            'No DRAFT correction for {0} {1} found'.format(src_kind, src_index)
        )


class TooManyDraftsFoundException(CorrectionException):
    def __init__(self, src_kind, src_index):
        super(TooManyDraftsFoundException, self).__init__(
            src_kind,
            src_index,
            'Too many DRAFT corrections for {0} {1}'.format(src_kind, src_index)
        )


class CorrectionVersionMismatchException(CorrectionException):
    def __init__(self, src_kind, src_index, correction_version, content_version):
        super(CorrectionVersionMismatchException, self).__init__(
            src_kind,
            src_index,
            "Correction version=({2}) and {0} {1} version=({3}) are not in sync".format(
                src_kind,
                src_index,
                correction_version,
                content_version)
        )
