__author__ = 'ramprax'

import datetime

from google.cloud import ndb
from google.cloud.ndb import polymodel


class PostContentSource(polymodel.PolyModel):
    """A source from which the content for daily posts are sourced"""
    name = ndb.StringProperty(required=True)
    author = ndb.StringProperty()
    post_name = ndb.StringProperty(required=True)
    num_available_posts = ndb.IntegerProperty(required=True)
    posted_count = ndb.IntegerProperty()
    posted_ratio = ndb.ComputedProperty(
        lambda self: ((1.0 * self.posted_count)/self.num_available_posts if self.num_available_posts else 0.0)
    )
    is_ready_to_mail = ndb.BooleanProperty()

    def get_source_name(self):
        return self.name

    def get_daily_post_name(self):
        return self.post_name

    @classmethod
    def fetch_by_post_name(cls, post_name):
        return PostContentSource.query(PostContentSource.post_name == post_name).get()


class PostIndex(ndb.Model):
    post_name = ndb.StringProperty(required=True, indexed=True)
    index = ndb.IntegerProperty(required=True, indexed=True)

    def content_source(self):
        return PostContentSource.fetch_by_post_name(self.post_name)


class PostContent(PostIndex):
    """A single post's content that is part of the content source"""
    title = ndb.StringProperty(required=True)
    text = ndb.TextProperty(required=True)
    html = ndb.TextProperty()
    version = ndb.IntegerProperty(required=True, indexed=True)

    def post_info(self):
        return PostInfo.query(PostInfo.post_name == self.post_name, PostInfo.index == self.index).get()

    def get_older_versions(self):
        return PostContentHistory.query(
            PostContentHistory.post_name == self.post_name,
            PostContentHistory.index == self.index,
            PostContentHistory.version < self.version
        ).order(PostContentHistory.version).fetch()

    @classmethod
    def fetch_by_post_name_index(cls, post_name, index):
        return PostContent.query(PostContent.post_name == post_name, PostContent.index == index).get()

    def get_or_make_html(self):
        if self.html is not None:
            return self.html

        if self.post_name == 'Day by Day':
            text_lines = self.text.split('\n')
            paras = [('<p>'+line+'</p>') for line in text_lines]
            return '\n'.join(paras)

        return self.text.replace('\n', '<br />')


class PostContentHistory(PostContent):
    version_end_datetime = ndb.DateTimeProperty(auto_now_add=True)


POST_MAILING_STATUS_DRAFT = 'DRAFT'
POST_MAILING_STATUS_SENT = 'SENT'
POST_MAILING_STATUS_RECEIVED = 'RECEIVED'


class PostInfo(PostIndex):
    """Info about a mailed post."""
    posted_count = ndb.IntegerProperty(indexed=True)
    last_posted = ndb.DateProperty()
    current_posting_status = ndb.StringProperty()
    posted_dates = ndb.DateProperty(repeated=True)
    days_since_last_post = ndb.ComputedProperty(
        lambda self: (datetime.date.today() - self.last_posted).days if self.last_posted else 9999,
        indexed=True
    )

    def content(self):
        return PostContent.query(PostContent.post_name == self.post_name, PostContent.index == self.index).get()

    def notify_msg_drafted(self):
        self.current_posting_status = POST_MAILING_STATUS_DRAFT
        return self.put()

    def notify_msg_sent(self):
        self.current_posting_status = POST_MAILING_STATUS_SENT
        return self.put()

    def notify_msg_received(self):
        if self.current_posting_status == POST_MAILING_STATUS_RECEIVED:
            return None
        self.current_posting_status = POST_MAILING_STATUS_RECEIVED
        self.last_posted = datetime.date.today()
        self.posted_dates.append(self.last_posted)
        self.posted_count += 1
        src = self.content_source()
        src.posted_count += 1
        src.put()
        return self.put()

    @classmethod
    def get_post_info(cls, post_name, index):
        return PostInfo.query(
            PostInfo.post_name == post_name,
            PostInfo.index == index).get()


# ramana_talks = PostContentSource(
#    name='Talks with Sri Ramana Maharshi',
#    author='Munagala Venkatramiah',
#    post_name='Talk',
#    num_available_posts=653
# )

CORRECTION_STATUS_DRAFT = 'DRAFT'
CORRECTION_STATUS_FINAL = 'FINAL'

class Correction(ndb.Model):
    src_kind = ndb.StringProperty()  # Which object(Talk/Letter/Recollection/etc) is this correction for
    src_index = ndb.IntegerProperty()  # Which index is being corrected
    content_version = ndb.IntegerProperty()  # Which version is this correction on top of?
    status = ndb.StringProperty()
    proposed_title = ndb.StringProperty()
    proposed_text = ndb.TextProperty()
    proposed_html = ndb.TextProperty()
    corrected_by = ndb.UserProperty()
    corrected_on = ndb.DateTimeProperty(auto_now=True)


class ContentArchive(ndb.Model):
    src_kind = ndb.StringProperty()
    src_index = ndb.IntegerProperty()
    archived_version = ndb.IntegerProperty()
    archived_title = ndb.StringProperty()
    archived_text_zip = ndb.BlobProperty()
    archived_html_zip = ndb.BlobProperty()
    archived_by = ndb.UserProperty()
    archived_on = ndb.DateTimeProperty(auto_now=True)
