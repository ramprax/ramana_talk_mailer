__author__ = 'ramprax'

import datetime
import logging
from random import sample, randint
from flask import render_template
from google.appengine.api.users import is_current_user_admin
from google.cloud import ndb

from rr_daily.common.content.models import PostInfo, PostContentSource, PostContent


def get_all_sources(is_test=False):
    src_qry = (
        PostContentSource.query() if is_test else
        PostContentSource.query(PostContentSource.is_ready_to_mail == True)
    )
    return [ s for s in src_qry.order(PostContentSource.name).fetch() ]


def get_all_sources_for_admin():
    if not is_current_user_admin():
        raise Exception('Access Denied')

    src_qry = PostContentSource.query()
    return [ s for s in src_qry.order(PostContentSource.name).fetch() ]


def get_all_sources_for_reading():
    src_qry = PostContentSource.query()
    return [ s for s in src_qry.order(PostContentSource.name).fetch() ]


def choose_today_post(is_test=False):
    src_qry = (
        PostContentSource.query() if is_test else
        PostContentSource.query(PostContentSource.is_ready_to_mail == True)
    )

    content_source = src_qry.order(PostContentSource.posted_ratio).get()

    logging.debug('Least posted source: {0}'.format(content_source.name))

    all_post_qry = PostInfo.query(
        PostInfo.post_name == content_source.post_name,
        ).order(PostInfo.posted_count)

    if all_post_qry.count() < content_source.num_available_posts:
        for i in range(content_source.num_available_posts):
            index = i + 1
            get_or_create_post_info(post_name=content_source.post_name, index=index)

    least_posted_post = all_post_qry.get()

    if least_posted_post is not None:
        logging.debug('Least posted post: {0} {1} - count {2}'.format(
            least_posted_post.post_name,
            least_posted_post.index,
            least_posted_post.posted_count)
        )
        min_count = least_posted_post.posted_count

        if min_count == 0:
            logging.debug('Chosen post: {0} {1} - count {2}'.format(
                least_posted_post.post_name,
                least_posted_post.index,
                least_posted_post.posted_count)
            )
            return least_posted_post

        logging.debug("There are no post info's with count zero")

        pi_indices = [
            x.index for x in all_post_qry.fetch()
        ]
        logging.debug('Already posted posts: {0}'.format(len(pi_indices)))

        if len(pi_indices) < content_source.num_available_posts:
            never_posted = [x for x in range(1, content_source.num_available_posts+1) if x not in pi_indices]
            logging.debug('Never posted posts: {0}'.format(len(never_posted)))

            new_index = sample(never_posted, 1)[0]

            pi = get_or_create_post_info(post_name=content_source.post_name, index=new_index)

            logging.debug('Chosen post: {0} {1} - count {2}'.format(
                pi.post_name,
                pi.index,
                pi.posted_count)
            )
            return pi

        logging.debug("All posts have been posted atleast once.")

        n_days = 180
        date_n_days_ago = datetime.date.today() - datetime.timedelta(days=n_days)

        pi_list = [
            pi for pi in PostInfo.query(
                PostInfo.post_name == content_source.post_name,
                PostInfo.last_posted < date_n_days_ago,
                PostInfo.posted_count == min_count
            ).fetch()
        ]

        logging.debug('Posts posted more than {0} days ago: {1}'.format(n_days, len(pi_list)))

        if pi_list:
            pi = sample(pi_list, 1)[0]
            logging.debug('Chosen post: {0} {1} - count {2}'.format(
                pi.post_name,
                pi.index,
                pi.posted_count)
            )
            return pi

    logging.debug('Fallback to just picking at random')

    idx = randint(1, content_source.num_available_posts)
    pi = get_or_create_post_info(post_name=content_source.post_name, index=idx)

    logging.debug('Chosen post: {0} {1} - count {2}'.format(
        pi.post_name,
        pi.index,
        pi.posted_count)
    )
    return pi


def get_post_info(post_name, index):
    pi = PostInfo.get_post_info(post_name, index)
    return pi


def get_or_create_post_info(post_name, index):
    pi = get_post_info(post_name, index)
    if pi is None:
        pi = PostInfo()
        pi.post_name = post_name
        pi.index = index
        pi.posted_count = 0
        pi.put()
    return pi


def get_post_info_from_key(msg_history_key):
    hist = ndb.Key(urlsafe=msg_history_key).get()
    return hist


def get_post_content(post_name, index):
    return PostContent.fetch_by_post_name_index(post_name, index)


def get_post_content_source(post_name):
    return PostContentSource.fetch_by_post_name(post_name)


EMAIL_SUBJECT_FORMAT = u"""{title} - {source_name}"""

PLAIN_TEXT_MSG_FORMAT = u"""
{source_name}

{one_day_text}
"""


def build_email_message(em, content):
    src = content.content_source()
    em.subject = EMAIL_SUBJECT_FORMAT.format(
        title=content.title,
        source_name=src.name
    )
    em.body = PLAIN_TEXT_MSG_FORMAT.format(
        source_name=src.name,
        one_day_text=content.text
    )
    em.html = render_template(
        'mail_content_template.html',
        source_name=src.name,
        one_day_html=content.get_or_make_html(),
        content_subject=em.subject,
        post_name=content.post_name,
        post_index=content.index
    )
    return em


def build_html_page_content(post_info):
    content_sources = get_all_sources_for_admin() if is_current_user_admin() else get_all_sources()

    return render_template(
        'today_page.html',
        post_name=post_info.post_name,
        post_index=post_info.index,
        content_sources=content_sources
    )


def get_most_recent_post():
    return PostInfo.query().order(-PostInfo.last_posted).get()


def get_most_recent_posts(num):
    return PostInfo.query().order(-PostInfo.last_posted).fetch(limit=num)
