import functools

from google.cloud.ndb import context as gcloud_context_module

from rr_daily.naan_yaar import ndb_client


def wrap_with_ndb_client_context(f):
    """
    Wrapper to create ndb.client.context if one is not available already.
    Usually done once per http-request via wsgi-wrapper
    """
    @functools.wraps(f)
    def _with_ndb_client_context(*args, **kwargs):
        ctx = gcloud_context_module.get_context(raise_context_error=False)
        if ctx:
            # Context already available
            return f(*args, **kwargs)
        else:
            with ndb_client.context() as ctx:
                return f(*args, **kwargs)

    return _with_ndb_client_context

