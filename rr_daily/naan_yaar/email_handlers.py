import re
import unicodedata

from rr_daily.common.content.posts import get_post_info_from_key, choose_today_post, build_email_message
from rr_daily.common.content.models import POST_MAILING_STATUS_SENT

__author__ = 'ramprax'

import logging
import email
from google.appengine.api import mail

from rr_daily.common.gae_helper import wrap_with_ndb_client_context
from rr_daily.common.mailing.models import EmailDetail

@wrap_with_ndb_client_context
def get_email_from_name(name):
    ed = EmailDetail.get_by_name(name)
    return ed.email if ed is not None else None

NAAN_YAAR_EMAIL_ID = get_email_from_name('naan.yaar')
NOBODY_EMAIL_ID = get_email_from_name('nobody')

def get_admin_email_ids():
    email_ids = []
    logging.info('Fetching admin email details.')
    for em in EmailDetail.query().fetch():
        if em.is_admin:
            email_ids.append(em.email)
    return email_ids


def get_recipient_email_ids():
    return [em.email for em in EmailDetail.query().fetch() if em.is_receiver and not em.is_test]


def get_test_recipient_email_ids():
    return [em.email for em in EmailDetail.query().fetch() if em.is_receiver and em.is_test]

def remove_recipients(email_ids, *black_list):
    ids = []
    for e in email_ids:
        found = False
        for arg in black_list:
            if e in arg or e == arg:
                found = True
                break
        if not found:
            ids.append(e)
    return ids


def extract_fields(message, *field_names):
    values = []
    for field_name in field_names:
        if field_name in message:
            values.append(message[field_name])
    return ' '.join(values)


def slugify_custom(value):
    """
    Normalizes string, converts to lowercase, removes non-alpha characters,
    and converts spaces to hyphens.
    Copied from django
    """
    # import unicodedata
    value = unicodedata.normalize('NFKD', value)  # .encode('ascii', 'ignore')
    value = re.sub(r'[^\w\s-]', '', value).strip().lower()
    return re.sub(r'[-\s]+', '-', value)


ADMIN_FWD_MAIL_FORMAT = """
The application 'ramana-talk-mailer.appspot.com' received an email
From: {0}
To: {1}
Subject: {2}
Summary
=======
{3}
=======
See attached original mail.
"""


def handle_incoming_mail(receiving_email_id, url_path, raw_data):
    original_message = email.message_from_string(raw_data)
    iem = mail.InboundEmailMessage(raw_data)  # For decoding subject & body

    dont_fwd_to_addresses = extract_fields(
        original_message,
        'from', 'to', 'cc', 'bcc', 'sender'  # , 'reply-to'
    )

    original_from = extract_fields(original_message, 'from')
    new_from = receiving_email_id

    if (NAAN_YAAR_EMAIL_ID in original_from) or (NOBODY_EMAIL_ID in original_from):
        logging.warning("Mail originally from us. Won't forward.")
        logging.info('url path is {0}'.format(url_path))
        if NOBODY_EMAIL_ID in url_path:
            # Handle case when this is a mail from cron-job or google-group
            do_cron_mail_book_keeping(original_message)
        return 'Loop detected.'

    original_subject = iem.subject  # extract_fields(original_message, 'subject')
    new_subject = ('FWD: '+original_subject if original_subject
                   else "Received mail at ramana-talk-mailer.appspot.com")

    admin_ids = get_admin_email_ids()
    new_to = remove_recipients(admin_ids, dont_fwd_to_addresses)
    original_to = extract_fields(original_message, 'to')

    mail_bodies = []
    for content_type, b in iem.bodies(content_type='text/plain'):
        text_body = b.decode()
        quoted_mail_stripped = '\n'.join([line for line in text_body.split('\n') if not line.startswith('> ')])
        mail_bodies.append(quoted_mail_stripped)

    summary = '*************\n'.join(mail_bodies)

    if new_to:
        logging.info('Forwarding mail to admin')

        original_msg_filename = slugify_custom(iem.sender)[:25] + '_' + slugify_custom(iem.subject)[:25]
        original_msg_filename = original_msg_filename + '.eml'

        mail.send_mail(
            sender=new_from,
            subject=new_subject,
            to=new_to,
            body=ADMIN_FWD_MAIL_FORMAT.format(
                original_from, original_to, original_subject.encode('utf8'), summary.encode('utf8')),
            attachments=[(original_msg_filename, raw_data)]
        )
    else:
        logging.warning('Loop detected')
    pass


def do_cron_mail_book_keeping(mail_message):
    msg_history_key = mail_message['References'] if 'References' in mail_message else None

    if not msg_history_key:
        logging.info('No msg_history_key in header')
        return

    hist = None

    try:
        hist = get_post_info_from_key(msg_history_key)
    except:
        logging.error('Unknown key: {0}'.format(msg_history_key))
        logging.error('Please mark the post as RECEIVED manually')

    if not hist:
        logging.error('Unknown msg_history_key: {0}'.format(msg_history_key))
        return

    logging.info('Received msg with msg_history_key: {0}'.format(msg_history_key))

    if hist.current_posting_status != POST_MAILING_STATUS_SENT:
        logging.error('Not expecting this message now. msg_history_key: {0}'.format(msg_history_key))
        return
    hist.notify_msg_received()

    logging.info('Done updating msg history for key: {0}'.format(msg_history_key))


def send_daily_ramana_mail(is_cron_job, is_test):
    pi = choose_today_post(is_test=is_test)
    message = build_email_message(mail.EmailMessage(), pi.content())

    email_sender = NAAN_YAAR_EMAIL_ID
    test_recipients = get_test_recipient_email_ids()
    recipients = test_recipients if is_test else (
        get_recipient_email_ids() if is_cron_job else [NOBODY_EMAIL_ID]
    )

    message.sender = email_sender
    message.to = recipients

    message.check_initialized()

    if is_test or is_cron_job:
        # Send mail only if in test(to test address) or in cron mode
        logging.info('Sending mail to {0} recipient(s).'.format(len(recipients)))

        if not is_test:
            # Should not create msg hist in test mode
            msg_history_key = pi.notify_msg_drafted()
            logging.info('msg_history_key = {0}'.format(msg_history_key))
            message.headers = {"References": msg_history_key.urlsafe().decode()}

        message.send()

        if not is_test:
            # Should not update msg hist in test mode
            pi.notify_msg_sent()

        logging.info('Done sending mail.')
        return 'Done sending mail.'
    else:
        logging.info('''Neither cron nor test mode. Won't send mail.''')
        result_string = u'''
                *** NO MODE ***
                Mail *not* sent
                Mail content:
                From: {0}
                To: {1}
                Subject: {2}
                Body: {3}
                Html: {4}
                '''.format(
            message.sender,
            str(message.to),
            message.subject,
            message.body,
            message.html
        )
        logging.info(result_string)
        return result_string
