(function(){
var decadd = function(sometxt) {
  var txtarr = sometxt.split(' ');
  var newtxt = txtarr.join('');

  return sometxt.replace("<dot>", '.').replace("<at>", '@').replace("<dot>", '.').replace("<dot>", '.');
}

function encadd() {
  var addarr = make_full_add_arr(
    make_id_part_arr(['naan', 'yaar']),
    make_fulldomarr(['ramana', 'talk', 'mailer'], ['appspotmail', 'com']));

  return addarr.join(' ');
}

function make_full_add_arr(idarr, fulldomarr) {
    var retarr = []
    for(var i = 0; i < idarr.length; i++) {
        retarr.push(idarr[i]);
    }
    retarr.push("<at>");
    for(var i = 0; i < fulldomarr.length; i++) {
        retarr.push(fulldomarr[i]);
    }
    return retarr;
}

function make_id_part_arr(idarr) {
  var retarr = [];
  for(var i = 0; i < idarr.length; i++) {
      if(i > 0) {
          retarr.push("<dot>");
      }
      retarr.push(idarr[i]);
  }
  return retarr
}

function make_app_str(apparr) {
    return apparr.join('-');
}

function make_fulldomarr(apparr, domarr) {
    var retarr = []
    retarr.push(make_app_str(apparr));
    for(var i = 0; i < domarr.length; i++) {
        retarr.push("<dot>");
        retarr.push(domarr[i]);
    }
    return retarr;
}

function copyAddValToClipboard() {
    var txt = $("#addval").text();
    var newtxt2 = decadd(txt);

    navigator.clipboard.writeText(newtxt2).then(function() {
        console.log("Copied to clipboard successfully!");
    }, function() {
        console.error("Unable to write to clipboard. :-(");
    });
    return false;
}

//$("#copy_button").click(copyAddValToClipboard);

$(document).ready(function() {
  $("#addval").text(encadd());
});

})();
