(function() {

var refresh_content = function(update_title_and_history) {

  var post_name = $('#post_name').val();
  var post_index = $('#post_index').val();

  $.getJSON( FETCH_CONTENT_URL+"?post_name="+post_name+"&index="+post_index, function( data ) {
    var post_content = data['post_content'];
    var content_source = data['content_source'];

    $('#source_name_heading').html(content_source['name']);
    $('#content_post_title').html(post_content['index']+' &ndash; '+post_content['title']);
    $('#content_html_data').html(post_content['html']);

    $('#num_available_posts').val(content_source['num_available_posts']);
    $('#post_name').val(post_content['post_name']);
    $('#post_index').val(post_content['index']);

    if(update_title_and_history) {
      var newURL = READING_PAGE_URL+"?post_name=" + post_name+"&index="+post_index;
      if(newURL != window.location) {
        document.title = ""+post_name+" "+post_index+" from "+content_source['name'];
        history.pushState({}, ""+post_name+" "+post_index+" from "+content_source['name'], newURL);
      }
    }
  });

}

var show_post = function(post_name, index) {
    $('#post_name').val(post_name);
    $('#post_index').val(index);

    refresh_content(true);
}

var get_new_index = function(increment) {
  var post_index = $('#post_index').val();
  var num_available_posts = $('#num_available_posts').val();

  var pi = parseInt(post_index);
  var max_index = parseInt(num_available_posts);

  var ni = pi + increment;
  if(ni < 1) {
    ni = 1;
  } else if(ni > max_index) {
    ni = max_index;
  }
  return ni;
}

var paginator_func_gen = function(increment) {
  return function() {
    var post_name = $('#post_name').val();
    var next_index = get_new_index(increment);

    show_post(post_name, next_index);
    return false;
  };
}

var next_post = paginator_func_gen(1);
var prev_post = paginator_func_gen(-1);
var next_5_post = paginator_func_gen(5);
var prev_5_post = paginator_func_gen(-5);

var first_post = function() {
  var post_name = $('#post_name').val();
  show_post(post_name, 1);
  return false;
}

var last_post = function() {
  var post_name = $('#post_name').val();
  var num_available_posts = $('#num_available_posts').val();
  show_post(post_name, num_available_posts);
  return false;
}

function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}

if(PAGE_NAME == 'today') {
    $(document).ready(function () {
        refresh_content(false);
    });
}

$("#first_button_top").click(first_post);
$("#prev_5_button_top").click(prev_5_post);
$("#prev_button_top").click(prev_post);
$("#next_button_top").click(next_post);
$("#next_5_button_top").click(next_5_post);
$("#last_button_top").click(last_post);

$("#first_button_bottom").click(first_post);
$("#prev_5_button_bottom").click(prev_5_post);
$("#prev_button_bottom").click(prev_post);
$("#next_button_bottom").click(next_post);
$("#next_5_button_bottom").click(next_5_post);
$("#last_button_bottom").click(last_post);

//$("#goto_button").click(goto_post);
//$("#random_button").click(random_post);

})();
