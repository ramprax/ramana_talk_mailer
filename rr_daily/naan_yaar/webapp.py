import datetime
import logging
from urllib.parse import urljoin

from rr_daily.common.appconfig import get_app_config

from rr_daily.common.content.posts import get_most_recent_post, build_html_page_content, get_most_recent_posts, \
    get_all_sources_for_admin, get_all_sources, get_post_content, get_all_sources_for_reading, get_post_info
from rr_daily.naan_yaar.content import content_loader

from flask import Flask, request, render_template, url_for, abort, flash, redirect, Response, jsonify

from flask_bootstrap import Bootstrap

from google.appengine.api import taskqueue
from google.cloud import ndb
from google.appengine.api.users import get_current_user, is_current_user_admin
from google.appengine.api import wrap_wsgi_app


from feedwerk.atom import AtomFeed

from rr_daily.common.gae_helper import wrap_with_ndb_client_context
from rr_daily.naan_yaar.email_handlers import NAAN_YAAR_EMAIL_ID, NOBODY_EMAIL_ID, handle_incoming_mail, send_daily_ramana_mail

import os

IS_IN_DEV_MODE = os.environ['SERVER_SOFTWARE'].startswith('Development')

app = Flask(__name__)
app.wsgi_app = wrap_with_ndb_client_context(wrap_wsgi_app(app.wsgi_app, use_deferred=True))

app.config['DEBUG'] = True
app.config['SECRET_KEY'] = get_app_config().app_secret_key

# Note: We don't need to call run() since our application is embedded within
# the App Engine WSGI application server.

bootstrap = Bootstrap()
bootstrap.init_app(app)

if IS_IN_DEV_MODE:
    import test
    @app.route('/admin_runtests')
    def runtests():
        result_gen = test.runtests()
        return Response(result_gen, mimetype='text/plain')


@app.route('/')
def get_main_page_content():
    content_sources = (get_all_sources_for_admin() if is_current_user_admin() else get_all_sources())
    return render_template('main.html', content_sources=content_sources)


@app.route('/_ah/bounce', methods=['GET', 'POST'])
def bounce_handler():
    """Handle mail bounce."""
    logging.error("Bounce args={0}".format(str(request.args)))
    logging.error("Bounce post={0}".format(str(request.form)))
    logging.error("Bounce data={0}".format(str(request.data)))
    return 'OK'


def invalid_receiving_email_id(receiving_email_id, raw_message_data):
    logging.error("Email message received at unknown email id: {0}".format(receiving_email_id))
    logging.error("Inbound mail data=\n{0}".format(str(raw_message_data)))
    if not raw_message_data:
        logging.error("Nothing in data")

    return 'Received with thanks'


@app.route('/_ah/mail/<receiving_email_id>', methods=['GET', 'POST'])
def inbound_mail_handler(receiving_email_id):
    """Handle incoming mail """
    raw_data = request.data

    logging.info("Inbound mail args=\n{0}".format(str(request.args)))
    logging.info("Inbound mail post=\n{0}".format(str(request.form)))
    logging.info("Inbound mail data=\n{0}".format(str(raw_data)))

    decoded_mail_data = raw_data.decode('utf-8')
    logging.info("Inbound mail data=\n{0}".format(str(decoded_mail_data)))

    if not (receiving_email_id == NAAN_YAAR_EMAIL_ID or
        receiving_email_id == NOBODY_EMAIL_ID):
        return invalid_receiving_email_id(receiving_email_id, decoded_mail_data)

    if not request.data:
        logging.warning("Nothing in data")
        return 'Received with thanks'

    handle_incoming_mail(receiving_email_id, request.path, decoded_mail_data)
    return 'Received with thanks'


@app.route('/subscribe')
def subscribe_handler():
    return render_template('subscription_redirect.html')


@app.route('/admin_daily_ramana_mail', methods=['GET', 'POST'])
def daily_ramana_mail():
    is_cron_job = True if request.headers.get('X-AppEngine-Cron') == 'true' else False

    is_test = False
    if request.args.get('test') and request.args.get('test') in 'yY':
        is_test = True

    r = send_daily_ramana_mail(is_cron_job, is_test)
    resp = r if r else 'Message sent.'

    if is_cron_job:
        return resp

    flash(resp)
    return redirect(url_for('show_admin_page'))


def get_welling_base64_image():
    return open('Welling_12X14.jpg.base64').read()


@app.route('/test_choose_random_content')
def test_choose_random_content():
    from rr_daily.common.content.posts import choose_today_post
    pi = choose_today_post()
    return '''Chosen from new content: {0} {1} count={2}'''.format(
        pi.post_name, pi.index, pi.posted_count
    )


@app.route('/test_get_current_user', methods=['GET', 'POST'])
def test_get_current_user():
    curr_user = get_current_user()
    return jsonify({'user': curr_user.nickname(), 'is_admin': is_current_user_admin()})


@app.route('/fetch_content')
def fetch_content():
    post_name = request.args.get('post_name')
    index = request.args.get('index')
    if not post_name:
        abort(404)

    if not index:
        abort(404)

    index = int(index)
    pc = get_post_content(post_name, index)
    if not pc:
        abort(404)
    pcs = pc.content_source()
    if not pcs:
        abort(404)
    post_content={
        'post_name': pc.post_name,
        'index': pc.index,
        'title': pc.title,
        'text': pc.text,
        'html': pc.get_or_make_html()
    }
    content_source={
        'name': pcs.name,
        'author': pcs.author,
        'num_available_posts': pcs.num_available_posts
    }
    return jsonify(post_content=post_content, content_source=content_source)


@app.route('/read')
def read_post():
    post_name = request.args.get('post_name')
    index = request.args.get('index')

    if not post_name:
        abort(404)

    if not index:
        index = 1
    else:
        index = int(index)
    logging.debug('post_name={0} index={1}'.format(post_name, index))
    pc = get_post_content(post_name, index)
    if not pc:
        abort(404)
    src = pc.content_source()
    if not src:
        abort(404)

    content_sources = get_all_sources_for_reading()

    return render_template(
        'reading_page.html',
        post_name=pc.post_name,
        post_title=pc.title,
        num_available_posts=src.num_available_posts,
        post_index=index,
        content_sources=content_sources,
        content_html_data=pc.get_or_make_html(),
        source_name_heading=src.name
    )


@app.route('/today')
def today_content():
    pi = get_most_recent_post()
    return build_html_page_content(pi)


def make_external(url):
    return urljoin(request.url_root, url)


@app.route('/today.atom')
def recent_today_feed():
    feed = AtomFeed("Today's Ramana page - Recent Posts",
                    feed_url=request.url, url=request.url_root)

    for post_info in get_most_recent_posts(15):
        con = post_info.content()
        feed.add(
            con.title,
            con.get_or_make_html(),
            content_type='html',
            author='Ramana',
            url=make_external(url_for('today_content')),
            updated=datetime.datetime.combine(post_info.last_posted, datetime.time(15, 17))
        )
    return feed.get_response()


@app.route('/load_post_contents', methods=['POST'])
def load_post_contents():
    if not is_current_user_admin():
        flash('Access denied')
    else:
        jsonfile = request.files['jsonfile']
        failure_list = content_loader.load_post_contents_from_json(jsonfile)
        if not failure_list:
            flash('Post contents loaded successfully', 'info')
        else:
            flash('Not all post content items could be saved', 'error')
            for p, i, m in failure_list:
                flash(p+'|'+str(i)+'|'+m, 'error')

    return redirect(url_for('load_correction'))


@app.route('/load_correction', methods=['GET', 'POST'])
def load_correction():
    if request.method == 'POST':
        daily_post_name = request.form['content_source']
        index = request.form['index']
        if index:
            index = int(index)
            textfile = request.files['textfile']
            htmlfile = request.files['htmlfile']
            to_render = content_loader.load_new_correction(daily_post_name, index, textfile, htmlfile)
            if to_render:
                return to_render
            else:
                flash('No correction given in input.', 'error')
        else:
            flash('No index given.', 'error')

    content_sources = (get_all_sources_for_admin() if is_current_user_admin() else get_all_sources())
    return render_template('load_correction.html', content_sources=content_sources)


@app.route('/load_correction_review', methods=['GET', 'POST'])
def review_correction():
    if request.method == 'POST':
        src = request.form['content_source']
        index = request.form['index']
        if index:
            index = int(index)
            ret = content_loader.load_draft_correction(src, index)
            if ret is not None:
                return ret
            else:
                flash('No draft correction found for given content index.')
        else:
            flash('No index given.', 'error')

    return redirect(url_for('load_correction'))


@app.route('/load_correction_discard', methods=['GET', 'POST'])
def discard_correction():
    if request.method == 'POST':
        correction_key = request.form['correction_id']
        content_loader.discard_correction(correction_key)
    return redirect(url_for('load_correction'))


@app.route('/load_correction_confirm', methods=['GET', 'POST'])
def confirm_correction():
    if request.method == 'POST':
        correction_key = request.form['correction_id']
        content_loader.confirm_correction(correction_key)
    return redirect(url_for('load_correction'))


@app.route('/admin_page', methods=['GET', 'POST'])
def show_admin_page():
    curr_user = get_current_user()
    content_sources = (get_all_sources_for_admin() if is_current_user_admin() else get_all_sources())

    return render_template('admin_page.html', user_name=curr_user.nickname(), content_sources=content_sources)


@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return 'Sorry, nothing at this URL. Try again later.', 404


@app.route('/admin_queue_content_migration_task', methods=['GET', 'POST'])
def admin_queue_content_migration_task():
    source_name = request.args.get('source_name')
    clean = request.args.get('clean')
    if not clean:
        clean = 'n'
    def add_task_to_queue():
        taskqueue.add(
            url='/admin_start_content_migration',
            params={
                'source_name': source_name,
                'clean': clean
            },
            transactional=True)
    ndb.transaction(add_task_to_queue)
    flash('Content Migrate  Task queued for source_name : {0}'.format(source_name))
    return redirect(url_for('show_admin_page'))


@app.route('/admin_start_content_migration', methods=['GET', 'POST'])
def admin_start_content_migration():
    if request.method == 'POST':
        source_name = request.form['source_name']
        clean = request.form['clean']
        clean = (clean in 'yY')
        #migrate_content_source(source_name, clean=clean)
        flash('Content Migrate finished for source_name : {0}'.format(source_name))
    return redirect(url_for('show_admin_page'))


@app.route('/admin_queue_post_info_migration_task', methods=['GET', 'POST'])
def admin_queue_post_info_migration_task():
    source_name = request.args.get('source_name')
    clean = request.args.get('clean')
    if not clean:
        clean = 'n'
    def add_task_to_queue():
        taskqueue.add(
            url='/admin_start_post_info_migration',
            params={
                'source_name': source_name,
                'clean': clean
            },
            transactional=True)
    ndb.transaction(add_task_to_queue)
    flash('Post Info Migrate Task queued for source_name : {0}'.format(source_name))
    return redirect(url_for('show_admin_page'))


@app.route('/admin_start_post_info_migration', methods=['GET', 'POST'])
def admin_start_post_info_migration():
    if request.method == 'POST':
        source_name = request.form['source_name']
        clean = request.form['clean']
        clean = (clean in 'yY')
        # migrate_post_infos(source_name, clean=clean)
        flash('Post Info Migrate finished for source_name : {0}'.format(source_name))
    return redirect(url_for('show_admin_page'))


@app.route('/admin_queue_clean_all_post_data_task', methods=['GET', 'POST'])
def admin_queue_clean_all_post_data_task():
    def add_task_to_queue():
        taskqueue.add(
            url='/admin_clean_all_post_data',
            transactional=True)
    ndb.transaction(add_task_to_queue)
    flash('Post Info Migrate Task queued for clean_all_post_data')
    return redirect(url_for('show_admin_page'))


@app.route('/admin_clean_all_post_data', methods=['GET', 'POST'])
def admin_clean_all_post_data():
    if request.method == 'POST':
        #clean_all_post_data()
        pass
    #flash('Post Info Migrate finished for clean_all_post_data')
    flash('Admin disabled')
    return redirect(url_for('show_admin_page'))


@app.route('/admin_touch_post', methods=['GET', 'POST'])
def admin_touch_post():
    post_name = request.args.get('post_name')
    index = int(request.args.get('index'))
    in_key = request.args.get('key')
    pi = get_post_info(post_name, index)
    actual_key = pi.key.urlsafe()
    if in_key == actual_key:
        key = pi.notify_msg_received().urlsafe()
        if key == actual_key:
            flash('Count updated for key {0}'.format(actual_key))
        else:
            flash('Did not update or something went wrong in update.', 'error')
    else:
        flash("Input key does not match with the post info entity's key.", 'error')
    return redirect(url_for('show_admin_page'))


@app.route('/reach_out', methods=['GET'])
def reach_out():
    content_sources = (get_all_sources_for_admin() if is_current_user_admin() else get_all_sources())
    return render_template('reach_out.html', content_sources=content_sources)
