__author__ = 'ramprax'

import json
import difflib
from random import randint

from rr_daily.common.content.posts import PostContent
from rr_daily.naan_yaar.content.content_processor import MAX_TALK, check_talk_no, TALK_TITLE_FMT, get_talk_text_from_file, \
    RAMANA_TALKS_FILE_NAME, read_all_recollections, read_all_letters
# from google.cloud import ndb


from rr_daily.naan_yaar.content.daily_content_source import RamanaTalk, RamanaRecollection, RamanaLetter
from rr_daily.common.content.corrections import *


def get_random_talk_number():
    return randint(1, MAX_TALK)


def get_talk_text(talk_no):
    check_talk_no(talk_no)

    talk_instance = get_talk_from_data_store(talk_no)

    if not talk_instance:
        logging.info('Did not find talk {0} in datastore. Loading from file.'.format(talk_no))
        talk_title = TALK_TITLE_FMT.format(talk_no)
        talk_text = get_talk_text_from_file(talk_no)
        put_talk_to_data_store(talk_no, talk_title, talk_text)
    else:
        talk_text = talk_instance.talk_text

    return talk_text


def get_talk_from_data_store(talk_no):
    check_talk_no(talk_no)
    talk_title = TALK_TITLE_FMT.format(talk_no)
    talk_instance = RamanaTalk.get_by_id(talk_title)
    return talk_instance


def put_talk_to_data_store(talk_no, talk_title, talk_text):
    check_talk_no(talk_no)
    talk_instance = RamanaTalk(
        key_name=talk_title,
        talk_index=talk_no,
        talk_title=talk_title,
        talk_text=ndb.TextProperty(talk_text, 'utf-8')
    )
    talk_instance.put()


def load_all_talks_into_data_store():
    logging.info('Reading talks file')
    full_talk_text = open(RAMANA_TALKS_FILE_NAME).read()
    logging.info('Done reading talks file')
    for i in range(MAX_TALK):
        talk_no = i + 1
        logging.info('Processing talk {0}'.format(talk_no))
        if get_talk_from_data_store(talk_no):
            logging.info('Found talk {0} already in data store'.format(talk_no))
            continue
        logging.info('Extracting talk {0}'.format(talk_no))
        talk_title = TALK_TITLE_FMT.format(talk_no)
        talk_start = full_talk_text.index(TALK_TITLE_FMT.format(talk_no))
        full_talk_text = full_talk_text[talk_start:]
        if talk_no < MAX_TALK:
            talk_end = full_talk_text.index(TALK_TITLE_FMT.format(talk_no+1))
            talk_text = full_talk_text[:talk_end]
        else:
            talk_text = full_talk_text
        logging.info('Done extracting talk {0}'.format(talk_no))
        logging.info('Putting talk {0} to datastore'.format(talk_no))
        put_talk_to_data_store(talk_no, talk_title, talk_text)
        logging.info('Done putting talk {0} to datastore'.format(talk_no))
    return ''


# For recollections loading
def load_all_recollections_into_datastore():
    for i, title, rtext in read_all_recollections():
        q = RamanaRecollection.query()
        q.filter(RamanaRecollection.index == i)
        recoll = q.get()
        if not recoll:
            recoll = RamanaRecollection(
                index=i,
                title=title.decode('utf-8'),
                text=ndb.TextProperty(rtext, 'utf-8')
            )
            recoll.put()
            logging.info('Put recollection {0}: {1} - {2} bytes: key={3}'.format(
                i, title, len(rtext), str(recoll.key)
            ))
    return 'Done'


# For letters loading
def load_all_letters_into_datastore():
    for i, d, title, ltext in read_all_letters():
        q = RamanaLetter.query()
        q.filter(RamanaLetter.index == i)
        let = q.get()
        if not let:
            let = RamanaLetter(
                index=i,
                title=title.decode('utf-8'),
                text=ndb.TextProperty(ltext, 'utf-8')
            )
            let.put()
            logging.info('Put letter {index}: Dated {d}, Titled {title}: {bytes} bytes: key={key}'.format(
                index=i,
                d=(d if d else ''),
                title=title,
                bytes=len(ltext),
                key=str(let.key)
            ))
    return 'Done'


def load_new_correction(daily_post_name, index, textfile, htmlfile):
    the_content = get_post_content(daily_post_name, index)
    content_src = the_content.content_source()

    old_text = the_content.text
    old_html = None

    old_html = the_content.html

    if old_text is None:
        old_text = ''
    if old_html is None:
        old_html = ''

    if textfile or htmlfile:
        new_text = None
        new_html = None
        if textfile:
            new_text = textfile.read().decode('utf-8')
            #the_content.
        if htmlfile:
            new_html = htmlfile.read().decode('utf-8')

        if new_text or new_html:
            correction_key = upload_new_correction(daily_post_name, index, new_text, new_html)

            # For showing the difference, we show old data as new data if
            # there is no new data given
            if new_text is None:
                new_text = old_text
            if new_html is None:
                new_html = old_html

            return generate_review_page(
                correction_key,
                daily_post_name,
                index,
                old_text,
                old_html,
                new_text,
                new_html
            )

    else:
        logging.warning('{0} {1} already has html content. Doing nothing'.format(content_src.get_source_name(), index))

    return None


def load_draft_correction(daily_post_name, index):
    correction = get_saved_draft_correction(daily_post_name, index)
    if correction is None:
        return None
    correction_key = correction.key()
    new_text = correction.proposed_text
    new_html = correction.proposed_html

    the_content = get_post_content(daily_post_name, index)
    content_src = the_content.content_source()
    old_text = the_content.text
    old_html = None

    old_html = the_content.html

    if old_text is None:
        old_text = ''
    if old_html is None:
        old_html = ''
    if new_text is None:
        new_text = old_text
    if new_html is None:
        new_html = old_html

    old_text = u'''{0}\n{1}\n{0}'''.format('*'*80, old_text)
    old_html = u'''{0}\n{1}\n{0}'''.format('*'*80, old_html)
    new_text = u'''{0}\n{1}\n{0}'''.format('*'*80, new_text)
    new_html = u'''{0}\n{1}\n{0}'''.format('*'*80, new_html)

    return generate_review_page(correction_key, daily_post_name, index, old_text, old_html, new_text, new_html)


def render_template(param, content_src_name, index, old_text, old_html, new_text, new_html, correction_id,
                    txt_diff_table, html_diff_table):
    pass


def generate_review_page(correction_key, content_src_name, index, old_text, old_html, new_text, new_html):
    differ = difflib.HtmlDiff(wrapcolumn=80)
    txt_diff_table = differ.make_table(old_text.split('\n'), new_text.split('\n'))
    html_diff_table = differ.make_table(old_html.split('\n'), new_html.split('\n'))

    return render_template(
        'review_correction.html',
        content_src_name=content_src_name,
        index=index,
        old_text=old_text,
        old_html=old_html,
        new_text=new_text,
        new_html=new_html,
        correction_id=str(correction_key),
        txt_diff_table=txt_diff_table,
        html_diff_table=html_diff_table
    )


def load_post_contents_from_json(jsonfile):
    content_json = json.load(jsonfile, encoding='utf-8')
    failures = []
    for i, data in enumerate(content_json):
        post_name = data['post_name']
        index = data['index']

        postContentSrcObj = get_post_content_source(post_name)
        if not postContentSrcObj:
            error_msg = 'Post content {0} {1} has no parent content source {0}'.format(post_name, str(index))
            logging.error(error_msg)
            failures.append((post_name, index, error_msg))
            continue

        postContentObj = get_post_content(post_name, index)
        if postContentObj is not None:
            # error_msg = 'Post content {0} {1} already exists - skipped loading'.format(post_name, str(index))
            # logging.error(error_msg)
            # failures.append((post_name, index, error_msg))
            # continue
            new_title = data['title']
            title_differs = new_title and postContentObj.title != new_title
            if title_differs:
                postContentObj.title = new_title

            new_text = data['text']
            text_differs = new_text and postContentObj.text != new_text
            if text_differs:
                postContentObj.text = new_text

            new_html = data.get('html')
            html_differs = new_html and postContentObj.html != new_html
            if html_differs:
                postContentObj.html = new_html

            if title_differs or text_differs or html_differs:
                postContentObj.version = postContentObj.version + 1
                postContentObj.put()
        else:
            postContentObj = PostContent()

            postContentObj.version = 1
            postContentObj.post_name = post_name
            postContentObj.index = index
            postContentObj.title = data['title']
            postContentObj.text = data['text']
            if data.get('html') is not None:
                postContentObj.html = data.get('html')

            postContentObj.put()

    return failures

