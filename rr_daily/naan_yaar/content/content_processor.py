__author__ = 'ramprax'

RAMANA_TALKS_FILE_NAME = 'resources/ramana_talks.txt'
MAX_TALK = 653
TALK_TITLE_FMT = 'Talk {0}.'


def check_talk_no(talk_no):
    if talk_no < 1 or talk_no > MAX_TALK:
        raise Exception('Invalid talk number {0}'.format(talk_no))


def get_talk_text_from_file(talk_no):
    check_talk_no(talk_no)

    talk_text = open(RAMANA_TALKS_FILE_NAME).read()
    talk_start = talk_text.index(TALK_TITLE_FMT.format(talk_no))
    talk_text = talk_text[talk_start:]
    if talk_no < MAX_TALK:
        talk_end = talk_text.index(TALK_TITLE_FMT.format(talk_no+1))
        talk_text = talk_text[:talk_end]
    return talk_text


def read_all_recollections():
    recoll_txt = open('resources/recollections_text_draft.txt').read()
    delimiter = '_______'
    i = 0
    for r in recoll_txt.split(delimiter):
        i += 1
        r = r.strip()  # .decode('utf-8')
        first_new_line = r.index('\n')
        title = r[:first_new_line]
        yield (i, title.strip(), r)


def read_all_letters():
    letter_txt = open('resources/letters_text_draft.txt').read()
    delimiter = '_______'
    i = 0
    for let in letter_txt.split(delimiter):
        i += 1
        let = let.strip()
        letter_date = None
        title = None
        for line in let.split('\n'):
            line = line.strip()
            if line:
                if i == 273:
                    title = line
                    break
                if not letter_date:
                    letter_date = line
                    continue
                if not title:
                    title = line
                    break

        yield i, letter_date, title, let


if __name__ == '__main__':
    #    for i, t, r in read_all_recollections():
    #        print 'Got recollection # {0} - {1} : {2} bytes'.format(i, t, len(r))
    for i, d, t, l in read_all_letters():
        print(i, d, t, len(l), 'bytes')

