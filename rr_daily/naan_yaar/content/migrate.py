import logging

from google.cloud import ndb

from rr_daily.common.content.models import PostContentSource, PostContent, PostInfo
from rr_daily.naan_yaar.content.daily_content_source import ALL_SOURCES


__author__ = 'ramprax'


#@ndb.transactional
def migrate_content_source(source_name, clean=False):
    for src in ALL_SOURCES:
        if src.get_source_name() != source_name:
            continue
        logging.info('source_name matched '+source_name)
        new_src = PostContentSource.query(PostContentSource.name == src.get_source_name()).get()
        if clean:
            logging.info('Cleaning source')
            clean_src(new_src)
            new_src = None
        if new_src is None:
            logging.info('Copying source')
            new_src = copy_to_new_src(src)
        copy_posts(src, new_src)


def migrate_post_infos(source_name, clean=False):
    for src in ALL_SOURCES:
        if src.get_source_name() != source_name:
            continue
        new_src = PostContentSource.query(PostContentSource.name == src.get_source_name()).get()
        if clean:
            clean_post_infos(new_src)
        copy_msg_hists(src, new_src)


def clean_src(new_src):
    ndb.delete_multi(
        PostContent.query(
            PostContent.post_name == new_src.post_name
        ).fetch(keys_only=True)
    )
    new_src.key.delete()


def clean_all_post_data():
    # ndb.delete_multi(
    #     PostContent.query().fetch(keys_only=True)
    # )
    # ndb.delete_multi(
    #     PostInfo.query().fetch(keys_only=True)
    # )
    # ndb.delete_multi(
    #     PostContentSource.query().fetch(keys_only=True)
    # )
    pass


def clean_post_infos(new_src):
    new_src.posted_count = 0
    new_src.put()
    ndb.delete_multi(
        PostInfo.query(
            PostContent.post_name == new_src.post_name
        ).fetch(keys_only=True)
    )


def copy_to_new_src(src):
    new_src = PostContentSource()
    new_src.name = src.get_source_name()
    new_src.post_name = src.get_daily_post_name()
    new_src.num_available_posts = src.get_max_index()
    new_src.posted_count = 0
    new_src.is_ready_to_mail = False
    k = new_src.put()
    logging.info('source saved with key = {0}'.format(str(k)))
    return new_src

def copy_posts(src, new_src):
    for old_post in src._source_model_class.query().fetch():
        new_post_content = PostContent.query(
            PostContent.post_name == new_src.post_name,
            PostContent.index == old_post.index
        ).get()
        if new_post_content is None:
            new_post_content = PostContent()
            new_post_content.post_name = new_src.post_name
            new_post_content.index = old_post.index
            new_post_content.title = old_post.title
            new_post_content.text = old_post.text
            if old_post.html is not None:
                new_post_content.html = old_post.html
            if old_post.version is not None:
                new_post_content.version = old_post.version
            else:
                new_post_content.version = 1
            new_post_content.put()

def copy_msg_hists(src, new_src):
    count = 0
    for mh in src._history_model_class.query().fetch():
        new_post_info = PostInfo.query(
            PostInfo.post_name == new_src.post_name,
            PostInfo.index == mh.index
        ).get()
        if new_post_info is None:
            new_post_info = PostInfo()
            new_post_info.post_name = new_src.post_name
            new_post_info.index = mh.index
            new_post_info.posted_count = 0
            new_post_info.posted_dates = []
            new_post_info.posted_count += mh.count
            count += mh.count
            if mh.last_mailed is not None:
                if new_post_info.last_posted is None or new_post_info.last_posted < mh.last_mailed:
                    new_post_info.last_posted = mh.last_mailed
                new_post_info.posted_dates.append(mh.last_mailed)
            new_post_info.put()
    new_src.posted_count += count
    new_src.put()
    # for pc in PostContent.query(PostContent.source_key == new_src.key).fetch():
    #     new_post_info = PostInfo.query(
    #         PostInfo.content_key == pc.key
    #     ).get()
    #     if new_post_info is None:
    #         new_post_info = PostInfo()
    #         new_post_info.source_key = new_src.key
    #         new_post_info.index = pc.index
    #         new_post_info.content_key = pc.key
    #         new_post_info.posted_count = 0
    #         new_post_info.posted_dates = []
    #         new_post_info.put()
