
import logging
import zlib

from random import randint, sample
import datetime
import heapq

from flask import render_template
from google.appengine.api.users import get_current_user

from google.cloud import ndb
from rr_daily.common.content.models import ContentArchive

MSG_STATUS_DRAFT = 'DRAFT'
MSG_STATUS_SENT = 'SENT'
MSG_STATUS_RECEIVED = 'RECEIVED'


class DailyContentSource(object):
    """
    This is a book or a scripture or chapter from which mail content
    is retrieved.
    """
    
    EMAIL_SUBJECT_FORMAT = u"""{title} - {source_name}"""
    #TITLE_FORMAT = None
    PLAIN_TEXT_MSG_FORMAT = u"""
{source_name}

{one_day_text}
"""

    def __init__(self, src_name, daily_post_name, max_index, src_ds_kind, src_model_cls, hist_ds_kind, hist_model_cls,
                 src_idx_col='index', src_title_col='title', src_text_col='text', src_html_col='html'):
        self._source_name = src_name  # 'Talks with Sri Ramana Maharshi', 'Letters from Sri Ramanasramam', etc.
        self._daily_post_name = daily_post_name  # 'Talk', 'Letter', 'Recollection', etc.

        self._source_datastore_kind = src_ds_kind
        self._source_model_class = src_model_cls
        #self._source_index_col = src_idx_col
        self._source_title_col = src_title_col
        self._source_text_col = src_text_col
        self._source_html_col = src_html_col

        self._history_datastore_kind = hist_ds_kind
        self._history_model_class = hist_model_cls
        self._history_count_col = hist_model_cls.count
        self._history_last_mailed_col = hist_model_cls.last_mailed
        self._history_msg_status_col = hist_model_cls.msg_status
        self.max_index = max_index

    def get_max_index(self):
        return self.max_index

    def get_daily_post_name(self):
        return self._daily_post_name
        
    def get_source_name(self):
        return self._source_name

    def get_datastore_kind(self):
        return self._source_datastore_kind

    def get_subject_for_email(self, title):
        return self.EMAIL_SUBJECT_FORMAT.format(
            title=title,
            source_name=self._source_name
        )

    # def get_index_col_name(self):
    #     return self._source_index_col

    def get_text_col_name(self):
        return self._source_text_col

    def get_html_col_name(self):
        return self._source_html_col

    def populate_email_message(self, message, index):
        if not index:
            logging.error('Got no {0} index'.format(self._daily_post_name))
            raise Exception('No {0} number index'.format(self._daily_post_name))

        logging.info('Fetching {0} index: {1}'.format(self._daily_post_name, index))
        row = self.get_entity_by_index(index)
        one_day_text = self.get_text_from_entity(row)
        title = row.title
        logging.info('Done fetching {0} index: {1}'.format(self._daily_post_name, index))

        message.subject = self.get_subject_for_email(title)
        message.body = self.PLAIN_TEXT_MSG_FORMAT.format(
            source_name=self._source_name,
            one_day_text=one_day_text
        )
        message.html = render_template(
            'mail_content_template.html',
            source_name=self._source_name,
            one_day_html=self.get_html_for_mail(row)
        )

        return message

    def get_html_page_content(self, index):
        return render_template(
            'today_page.html',
            source_name=self._source_name,
            one_day_html=self.get_html_for_mail(self.get_entity_by_index(index))
        )

    def get_html_for_mail(self, entity):
        one_day_html = self.get_html_from_entity(entity)

        if not one_day_html:
            one_day_text = self.get_text_from_entity(entity)
            one_day_html = one_day_text.replace("\n", "<br />")

        return one_day_html

    def check_entity_kind(self, entity):
        if entity.kind() != self._source_datastore_kind:
            raise Exception('Invalid entity kind. Expected kind {0}. Got {1}.'.format(
                self._source_datastore_kind, entity.kind()))

    def get_html_from_entity(self, entity):
        self.check_entity_kind(entity)
        return entity.html

    def get_text_from_entity(self, entity):
        self.check_entity_kind(entity)
        return entity.text

    def get_title_from_entity(self, entity):
        self.check_entity_kind(entity)
        return entity.title

    def get_text_by_index(self, index):
        self.check_index(index)

        row = self.get_entity_by_index(index)
        if not row:
            self.raise_error('Did not find {0} {1} in datastore.'.format(self._daily_post_name, index))

        return self.get_text_from_entity(row)

    def get_title_by_index(self, index):
        self.check_index(index)

        row = self.get_entity_by_index(index)
        if not row:
            self.raise_error('Did not find {0} {1} in datastore.'.format(self._daily_post_name, index))

        return self.get_title_from_entity(row)

    def get_html_by_index(self, index):
        self.check_index(index)

        row = self.get_entity_by_index(index)
        if not row:
            self.raise_error('Did not find {0} {1} in datastore.'.format(self._daily_post_name, index))

        return self.get_html_from_entity(row)

    def get_entity_by_index(self, index):
        self.check_index(index)
        row = self._source_model_class.get_by_index(index)
        return row

    def get_random_row_index(self):
        return randint(1, self.max_index)

    def check_index(self, index):
        if index < 1 or index > self.max_index:
            self.raise_error('Invalid {0} index: {1}'.format(self._source_name, index))

    # @db.transactional
    def get_or_create_msg_history(self, index, should_create=True):
        msg_history = self._history_model_class.get_by_index(index)
        if msg_history is None and should_create:
            msg_history = self._history_model_class()
            msg_history.index = index
            msg_history.count = 0
            msg_history.msg_status = MSG_STATUS_DRAFT
            msg_history.put()
            logging.info('Created {0} for index {1}'.format(self._history_datastore_kind, index))
        else:
            logging.info('Found {0} for index {1}'.format(self._history_datastore_kind, index))
        return msg_history

    def get_index_to_mail(self):
        """Choose a talk number that is fit for mailing today"""
        q = self._history_model_class.all()
        q.filter(self._history_model_class.count == 0)  # count = 0
        msg_hist = q.get()
        if msg_hist:
            logging.info('Found {0} {1} with count 0'.format(
                self._daily_post_name,
                msg_hist.index))
            return msg_hist.index

        # If history has no talks with count = 0
        # Look for talks that have never got posted and don't have a history record
        q = self._history_model_class.all()
        row_indexes = [h.index for h in q.run()]

        logging.info('Number of already posted rows is {0}'.format(len(row_indexes)))
        logging.debug('The row indexes already posted are {0}'.format(
            ','.join(str(x) for x in row_indexes))
        )
        if len(row_indexes) < self.max_index:
            never_posted_rows = [(x+1) for x in range(self.max_index) if (x+1) not in row_indexes]
            logging.info(
                'Number of rows never posted is {0}'.format(
                    len(never_posted_rows)
                )
            )
            logging.debug('The row indexes never posted are {0}'.format(
                ','.join(str(x) for x in never_posted_rows))
            )
            if never_posted_rows:
                rand_row_index = sample(never_posted_rows, 1)[0]
                logging.info('Chose row {0}'.format(rand_row_index))
                return rand_row_index

        # All talks have been posted at least once

        # Get the least number of times any talk has been posted
        n_days = 180
        date_n_days_ago = datetime.date.today() - datetime.timedelta(days=n_days)
        logging.info('{0} days ago it was {1}'.format(n_days, date_n_days_ago))

        q = self._history_model_class.all()
        q.filter(self._history_model_class.last_mailed < date_n_days_ago)
        q.order(self._history_count_col)
        res = q.get()
        min_count = res.count

        logging.info('Minimum count = '.format(min_count))

        # Look for the least frequently posted talks
        # and not posted within the last N days
        q = self._history_model_class.all()
        q.filter(self._history_model_class.last_mailed < date_n_days_ago)
        q.filter(self._history_model_class.count == min_count)

        least_freq_rows = [h.index for h in q.run()]
        logging.info(
            'Number of least frequently posted rows is '.format(
                len(least_freq_rows)
            )
        )
        return sample(least_freq_rows, 1)[0]

    def has_anything_been_posted(self):
        q = self._history_model_class.all()
        q.filter(self._history_model_class.count > 0)
        return not(not(q.get()))

    def get_msg_history_by_key(self, msg_history_key):
        db_key = ndb.Key(msg_history_key)
        if db_key.kind() != self._history_datastore_kind:
            self.raise_error(
                'Message history key kind mismatch for key: "{key}". Expected {expected}, got {got}'.format(
                    key=msg_history_key,
                    expected=self._history_datastore_kind,
                    got=db_key.kind()
                )
            )
        return self._history_model_class.get_by_id(db_key.id())

    def get_index_sent_on(self, dt):
        q = self._history_model_class.all()
        q.filter(self._history_model_class.last_mailed == dt)
        mh = q.get()
        if mh:
            return mh.index

        return None

    def get_row_by_key(self, key):
        db_key = ndb.Key(key)
        if db_key.kind() != self._source_datastore_kind:
            self.raise_error(
                'Message history key kind mismatch for key: "{key}". Expected {expected}, got {got}'.format(
                    key=key,
                    expected=self._source_datastore_kind,
                    got=db_key.kind()
                )
            )
        return self._source_model_class.get_by_id(db_key.id())

    def load_correction(self, index, new_title, new_text, new_html, correction_on_version):

        the_content = self.get_entity_by_index(index)

        if the_content.version != correction_on_version:
            self.raise_error('Version mismatch between content and correction for {0} {1}'.format(
                self.get_source_name(), index
            ))

        if not the_content:
            self.raise_error('No {0} for index {1}'.format(self.get_source_name(), index))

        if new_text or new_html or new_title:
            # Copy content from content table to archive table
            archived_content = retire_current_version(self._source_datastore_kind, index)
            if new_text:
                the_content.text = new_text
                logging.info('Saving {0} {1} text: {2} bytes'.format(self.get_source_name(), index, len(new_text)))
            if new_html:
                if hasattr(the_content, self._source_html_col):
                    setattr(the_content, self._source_html_col, ndb.TextProperty(new_html, 'utf-8'))
                    logging.info('Saving {0} {1} html: {2} bytes'.format(self.get_source_name(), index, len(new_html)))
                else:
                    logging.warning('{0} {1} - does not have a html field'.format(self.get_source_name(), index))
            if new_title:
                the_content.title_col = new_title
                logging.info('Saving {0} {1} title: {2} bytes'.format(self.get_source_name(), index, len(new_title)))
            the_content.version += 1
            the_content.put()
            return the_content
        else:
            logging.warning('{0} {1} - no correction given. Doing nothing'.format(self.get_source_name(), index))
            return None

    @staticmethod
    def raise_error(error_msg):
        logging.error(error_msg)
        raise Exception(error_msg)


class RamanaTalk(ndb.Model):
    talk_index = ndb.IntegerProperty()
    talk_title = ndb.StringProperty()
    talk_text = ndb.TextProperty()
    html = ndb.TextProperty()
    version = ndb.IntegerProperty()

    @classmethod
    def get_content_src(cls):
        return ramana_talks

    @property
    def index(self):
        #logging.debug("Getting index value")
        return self.talk_index

    @index.setter
    def index(self, value):
        #logging.debug("Setting index value")
        self.talk_index = value

    @property
    def title(self):
        #logging.debug("Getting title value")
        return self.talk_title

    @title.setter
    def title(self, value):
        #logging.debug("Setting title value")
        self.talk_title = value

    @property
    def text(self):
        #logging.debug("Getting text value")
        return self.talk_text

    @text.setter
    def text(self, value):
        #logging.debug("Setting text value")
        self.talk_text = value

    @classmethod
    def get_by_index(cls, index):
        q = cls.query()
        q.filter(cls.talk_index == index)
        row = q.get()
        return row


class RamanaTalkMsgHistory(ndb.Model):
    talk_index = ndb.IntegerProperty(indexed=True)
    count = ndb.IntegerProperty(indexed=True)
    last_mailed = ndb.DateProperty()
    msg_status = ndb.StringProperty()

    def get_entity(self):
        return ramana_talks.get_entity_by_index(self.talk_index)

    @property
    def index(self):
        #logging.debug("Getting value")
        return self.talk_index

    @index.setter
    def index(self, value):
        #logging.debug("Setting value")
        self.talk_index = value

    @classmethod
    def get_by_index(cls, index):
        q = cls.query()
        q.filter(cls.talk_index == index)
        row = q.get()
        return row


ramana_talks = DailyContentSource(
    src_name='Talks with Sri Ramana Maharshi',
    daily_post_name='Talk',
    max_index=653,
    src_ds_kind='RamanaTalk',
    src_model_cls=RamanaTalk,
    hist_ds_kind='RamanaTalkMsgHistory',
    hist_model_cls=RamanaTalkMsgHistory,
    src_idx_col='talk_index',
    src_title_col='talk_title',
    src_text_col='talk_text'
)


class RamanaLetter(ndb.Model):
    index = ndb.IntegerProperty()
    title = ndb.StringProperty()
    text = ndb.TextProperty()
    version = ndb.IntegerProperty()
    html = ndb.TextProperty()

    @classmethod
    def get_content_src(cls):
        return ramana_letters

    @classmethod
    def get_by_index(cls, index):
        q = cls.query()
        q.filter(cls.index == index)
        row = q.get()
        return row


class RamanaLetterMsgHistory(ndb.Model):
    index = ndb.IntegerProperty(indexed=True)
    count = ndb.IntegerProperty(indexed=True)
    last_mailed = ndb.DateProperty()
    msg_status = ndb.StringProperty()

    def get_entity(self):
        return ramana_letters.get_entity_by_index(self.index)

    @classmethod
    def get_by_index(cls, index):
        q = cls.query()
        q.filter(cls.index == index)
        row = q.get()
        return row

ramana_letters = DailyContentSource(
    src_name='Letters from Sri Ramanasramam',
    daily_post_name='Letter',
    max_index=273,
    src_ds_kind='RamanaLetter',
    src_model_cls=RamanaLetter,
    hist_ds_kind='RamanaLetterMsgHistory',
    hist_model_cls=RamanaLetterMsgHistory
)


class RamanaRecollection(ndb.Model):
    index = ndb.IntegerProperty()
    title = ndb.StringProperty()
    text = ndb.TextProperty()
    version = ndb.IntegerProperty()
    html = ndb.TextProperty()

    @classmethod
    def get_content_src(cls):
        return ramana_recollections

    @classmethod
    def get_by_index(cls, index):
        q = cls.query()
        q.filter(cls.index == index)
        row = q.get()
        return row

class RamanaRecollectionMsgHistory(ndb.Model):
    index = ndb.IntegerProperty(indexed=True)
    count = ndb.IntegerProperty(indexed=True)
    last_mailed = ndb.DateProperty()
    msg_status = ndb.StringProperty()

    def get_entity(self):
        return ramana_recollections.get_entity_by_index(self.index)

    @classmethod
    def get_by_index(cls, index):
        q = cls.query()
        q.filter(cls.index == index)
        row = q.get()
        return row

ramana_recollections = DailyContentSource(
    src_name='Recollections of Sri Ramanasramam',
    daily_post_name='Recollection',
    max_index=28,
    src_ds_kind='RamanaRecollection',
    src_model_cls=RamanaRecollection,
    hist_ds_kind='RamanaRecollectionMsgHistory',
    hist_model_cls=RamanaRecollectionMsgHistory
)


_KIND_TO_CONTENT_SOURCE_DICT = {
    'RamanaTalk': ramana_talks,
    'RamanaTalkMsgHistory': ramana_talks,
    'RamanaLetter': ramana_letters,
    'RamanaLetterMsgHistory': ramana_letters,
    'RamanaRecollection': ramana_recollections,
    'RamanaRecollectionMsgHistory': ramana_recollections
}
ALL_SOURCES = [
    ramana_talks,
    ramana_letters,
    ramana_recollections
]
_ALL_HISTORY_CLS = [
    RamanaTalkMsgHistory,
    RamanaLetterMsgHistory,
    RamanaRecollectionMsgHistory
]


def get_content_src_from_name(name):
    return _KIND_TO_CONTENT_SOURCE_DICT[name]


def get_msg_history_by_key(msg_history_key):
    db_key = ndb.Key(msg_history_key)
    content_src = _KIND_TO_CONTENT_SOURCE_DICT[db_key.kind()]
    return content_src.get_msg_history_by_key(msg_history_key)


#@db.transactional
def choose_today_content_source():
    """
    Choose between Talks, Letters and Recollections.
    """
    day = datetime.date.today().day
    logging.info('Got day={day}'.format(day=day))

    # If today is the eleventh of the month:
    if datetime.date.today().day == 11:
        recollection_index = ramana_recollections.get_index_to_mail()
        r_hist = ramana_recollections.get_or_create_msg_history(recollection_index)
        logging.info('Returning ramana_recollections index {0}'.format(recollection_index))
        return ramana_recollections, recollection_index, r_hist

    # Pick a number from 1 to (653+273)
    # If the number is greater than 653, return ramana_letters
    # Else return ramana_talks
    max_items = ramana_talks.max_index + ramana_letters.max_index
    r_index = randint(1, max_items)

    if r_index <= ramana_talks.max_index:
        talk_index = ramana_talks.get_index_to_mail()
        t_hist = ramana_talks.get_or_create_msg_history(talk_index)
        return ramana_talks, talk_index, t_hist

    letter_index = ramana_letters.get_index_to_mail()
    l_hist = ramana_letters.get_or_create_msg_history(letter_index)
    return ramana_letters, letter_index, l_hist


def has_anything_been_posted():
    for csrc in ALL_SOURCES:
        if csrc.has_anything_been_posted():
            return True
    return False


def get_content_sent_on(dt):
    for csrc in ALL_SOURCES:
        idx = csrc.get_index_sent_on(dt)
        if idx:
            return csrc, idx
    return None, None


def get_most_recently_sent_content():
    if not has_anything_been_posted():
        return None, None

    today = datetime.date.today()
    src = None
    idx = None
    while idx is None:
        src, idx = get_content_sent_on(today)
        today = today - datetime.timedelta(days=1)  # go to previous day

    return src, idx


def notify_msg_drafted(msg_history):
    msg_history.msg_status = MSG_STATUS_DRAFT  # 'DRAFT'
    msg_history.put()
    return msg_history.key()


def notify_msg_sent(msg_history):
    msg_history.msg_status = MSG_STATUS_SENT  # 'SENT'
    msg_history.put()


def notify_msg_received(msg_history):
    msg_history.last_mailed = datetime.date.today()
    count = msg_history.count
    logging.info('Count before = {0}'.format(count))
    count += 1
    msg_history.count = count
    msg_history.msg_status = MSG_STATUS_RECEIVED  # 'RECEIVED'
    msg_history.put()
    logging.info('Count after = {0}'.format(msg_history.count))


def retire_current_version(src_kind, src_index):
    content_src = get_content_src_from_name(src_kind)
    the_content = content_src.get_entity_by_index(src_index)
    q = ContentArchive.query()
    q.filter(ContentArchive.src_kind == src_kind)
    q.filter(ContentArchive.src_index == src_index)
    q.order(-ContentArchive.archived_version)
    last_archived = q.get()
    if last_archived.archived_version >= the_content.version:
        raise Exception("Version of {0} {1} is too old".format(src_kind, src_index))

    new_archive = ContentArchive(
        src_kind=src_kind,
        src_index=src_index,
        archived_version=the_content.version,
        archived_by=get_current_user(),
        )
    orig_text = content_src.get_text_from_entity(the_content)
    zipped_text = zlib.compress(orig_text)

    orig_html = content_src.get_html_from_entity(the_content)
    zipped_html = None
    if orig_html is not None:
        zipped_html = zlib.compress(orig_html)
    title = content_src.get_title_from_entity(the_content)

    new_archive.archived_title = title
    new_archive.archived_text_zip = zipped_text
    new_archive.archived_html_zip = zipped_html
    new_archive.put()
    logging.info('Created archive for {src_kind} {src_index}: key={archive_key}'.format(
        src_kind=src_kind,
        src_index=src_index,
        archive_key=str(new_archive.key)
    ))
    return new_archive


def get_recent_posts(n):
    hist_heap = []
    for hist_cls in _ALL_HISTORY_CLS:
        for hist in hist_cls.query().filter(hist_cls.last_mailed != None).order(-hist_cls.last_mailed).run(limit=n):
            if len(hist_heap) < n:
                heapq.heappush(hist_heap, (hist.last_mailed, hist))
            else:
                heapq.heappushpop(hist_heap, (hist.last_mailed, hist))
    ordered = [ heapq.heappop(hist_heap)[1] for i in range(len(hist_heap)) ]
    return reversed(ordered)
