from google.cloud import ndb

# We want to create this globally once per python process
ndb_client = ndb.Client()
