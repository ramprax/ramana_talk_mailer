from random import randint

from rr_daily.thakur_says.db import run_query_on_gospel_db


class InvalidSection(Exception):
    ...


def check_section_index(sid):
    if 1 <= sid <= get_section_count():
        return True
    else:
        raise InvalidSection(f'Invalid section number: {sid}')


def check_chapter_section(chapter_no, section_no):
    section_index = get_section_index(chapter_no, section_no)
    if section_index:
        return True
    else:
        raise InvalidSection(f'Could not find section index for chapter({chapter_no}) section({section_no})')


def get_random_section_index():
    num_sections = get_section_count()
    return randint(1, num_sections)


def get_section_count():
    q = 'select count(1) from section'
    p = []
    res = run_query_on_gospel_db(q, p)
    num_sections = res[0][0]
    return num_sections


def get_chapter_count():
    q = 'select count(1) from chapter'
    p = []
    res = run_query_on_gospel_db(q, p)
    num_chapters = res[0][0]
    return num_chapters


def get_gospel_section_content(section_index):
    check_section_index(section_index)
    q = '''SELECT
            b.chapter_id, b.chapter_name,
            a.section_no, a.section_heading, a.section_content
           FROM
            section a, chapter b
           WHERE
            a.rowid = ? AND
            a.chapter_id = b.chapter_id             
    '''
    p = [section_index]

    return run_query_on_gospel_db(q, p)[0]


def get_section_index(chapter_no, section_no):
    q = '''SELECT
            a.rowid
           FROM
            section a, chapter b
           WHERE
            a.chapter_id = b.chapter_id AND
            b.rowid = ? AND
            a.section_no = ?             
    '''
    p = [int(chapter_no), int(section_no)]

    res = run_query_on_gospel_db(q, p)
    if not res:
        raise InvalidSection(f'Could find chapter({chapter_no}) section({section_no})')

    return res[0][0]


def get_chapter_no_section_no(section_index):
    check_section_index(section_index)
    q = '''SELECT
            b.rowid AS chapter_no, a.section_no
           FROM
            section a, chapter b
           WHERE
            a.chapter_id = b.chapter_id AND
            a.rowid = ?            
    '''
    p = [int(section_index)]

    res = run_query_on_gospel_db(q, p)
    if not res:
        raise InvalidSection(f'Could find chapter & section for given section_index({section_index})')

    chapter_no, section_no = res[0]
    return int(chapter_no), int(section_no)


def get_previous_section_chsec(chapter_no, section_no):
    cur_section_index = get_section_index(chapter_no, section_no)  # Sanity check
    if section_no > 1:
        return int(chapter_no), section_no - 1
    if chapter_no > 1:
        return get_chapter_end_chsec(chapter_no - 1)

    return int(chapter_no), int(section_no)


def get_next_section_chsec(chapter_no, section_no):
    cur_section_index = get_section_index(chapter_no, section_no)

    _, last_section = get_chapter_end_chsec(chapter_no)
    if section_no < last_section:
        return int(chapter_no), int (section_no + 1)

    last_chapter = get_chapter_count()
    if chapter_no < last_chapter:
        return get_chapter_beginning_chsec(chapter_no + 1)

    return int(chapter_no), int(section_no)


def get_previous_chapter_chsec(chapter_no):
    chapter_no = chapter_no - 1 if chapter_no > 1 else chapter_no
    section_no = 1
    return get_chapter_no_section_no(get_section_index(chapter_no, section_no))


def get_chapter_beginning_chsec(chapter_no):
    section_no = 1
    return get_chapter_no_section_no(get_section_index(chapter_no, section_no))


def get_next_chapter_chsec(chapter_no):
    max_chapter = get_chapter_count()
    if chapter_no < max_chapter:
        chapter_no = chapter_no + 1
    section_no = 1
    return get_chapter_no_section_no(get_section_index(chapter_no, section_no))


def get_chapter_end_chsec(chapter_no):
    q = '''
        SELECT
         max(a.section_no)
        FROM
         section a
        WHERE
         a.chapter_id = (
          SELECT b.chapter_id FROM chapter b WHERE b.rowid = ?
         ) 
    '''
    p = [int(chapter_no)]

    res = run_query_on_gospel_db(q, p)
    section_no = int(res[0][0])
    return int(chapter_no), section_no

