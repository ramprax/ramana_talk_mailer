import sqlite3
from os.path import join, dirname, abspath


def get_gospel_db_file_path():
    db_file = join(dirname(dirname(dirname(abspath(__file__)))), 'resources', 'gospel.db')
    return db_file


def run_query_on_gospel_db(q, p):
    db_file = get_gospel_db_file_path()
    with sqlite3.connect(f'file:{db_file}?mode=ro', uri=True) as cnx:
        cur = cnx.cursor()
        cur.execute(q, p)
        res = cur.fetchall()
        cur.close()
        return res