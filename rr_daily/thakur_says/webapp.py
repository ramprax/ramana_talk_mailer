import os
import logging

from flask import Flask, request, render_template
from flask_bootstrap import Bootstrap
from google.appengine.api import wrap_wsgi_app

from rr_daily.common.appconfig import get_app_config
from rr_daily.common.gae_helper import wrap_with_ndb_client_context

IS_IN_DEV_MODE = os.environ['SERVER_SOFTWARE'].startswith('Development')

app = Flask(__name__)
app.wsgi_app = wrap_with_ndb_client_context(wrap_wsgi_app(app.wsgi_app, use_deferred=True))

app.config['DEBUG'] = True
app.config['SECRET_KEY'] = get_app_config().app_secret_key

# Note: We don't need to call run() since our application is embedded within
# the App Engine WSGI application server.

bootstrap = Bootstrap()
bootstrap.init_app(app)

from rr_daily.thakur_says.gospel_page import gospel_page_app

app.register_blueprint(gospel_page_app, url_prefix='/gospel')


@app.route('/', methods=['GET'])
def index():
    return render_template('thakur-says-main.html')

#
# @app.route('/')
# def get_main_page_content():
#     return 'OK'


@app.route('/_ah/bounce', methods=['GET', 'POST'])
def bounce_handler():
    """Handle mail bounce."""
    logging.error("Bounce args={0}".format(str(request.args)))
    logging.error("Bounce post={0}".format(str(request.form)))
    logging.error("Bounce data={0}".format(str(request.data)))
    return 'OK'


@app.route('/_ah/mail/<receiving_email_id>', methods=['GET', 'POST'])
def inbound_mail_handler(receiving_email_id):
    """Handle incoming mail """
    raw_data = request.data

    logging.info("Inbound mail args=\n{0}".format(str(request.args)))
    logging.info("Inbound mail post=\n{0}".format(str(request.form)))
    logging.info("Inbound mail data=\n{0}".format(str(raw_data)))

    decoded_mail_data = raw_data.decode('utf-8')
    logging.info("Inbound mail data=\n{0}".format(str(decoded_mail_data)))

    if not request.data:
        logging.warning("Nothing in data")
        return 'Received with thanks'

    return 'Received with thanks'

