import flask
from flask import render_template, url_for, redirect
from markupsafe import Markup

from rr_daily.thakur_says.gospel import get_gospel_section_content, get_section_index, get_previous_section_chsec, get_next_section_chsec, \
    get_chapter_beginning_chsec, get_chapter_end_chsec, get_previous_chapter_chsec, get_next_chapter_chsec, \
    check_chapter_section, get_random_section_index, get_chapter_no_section_no

gospel_page_app = flask.Blueprint('gospel_page', __name__)


@gospel_page_app.route('/<int:chapter_no>/<int:section_no>', methods=['GET'])
# @gospel_page_app.route('/<int:chapter_no>', methods=['GET'])
# @gospel_page_app.route('/', methods=['GET'])
def read_gospel_section(chapter_no: int = 1, section_no: int = 1):
    # check_chapter_section(chapter_no, section_no)
    section_index = get_section_index(chapter_no, section_no)
    scontent_info = get_gospel_section_content(section_index)
    chapter_id, chapter_title, section_no, section_heading, section_content = scontent_info
    section_text = section_content.split('\n')

    markupsafe_section_text = Markup('')

    is_in_song = False
    song_lines = None
    for section_text_p in section_text:
        if section_text_p.startswith('    '):
            is_in_song = True
            if song_lines is None:
                song_lines = Markup('<div>%s</div>') % section_text_p
            else:
                song_lines += Markup('<div>%s</div>') % section_text_p
        else:
            is_in_song = False
            if song_lines is not None:
                song_block = Markup('<div class="song_block">%s</div>') % song_lines
                markupsafe_section_text += song_block
                song_block = song_lines = None
            markupsafe_section_text += (Markup('<p>%s</p>') % section_text_p)

    markupsafe_section_text = (markupsafe_section_text.
        replace('(__', Markup('<i>')).
        replace('__)', Markup('</i>'))
    )

    markupsafe_section_heading = Markup('%s') % section_heading
    markupsafe_section_heading = (markupsafe_section_heading.
        replace('(__', Markup('<i>')).
        replace('__)', Markup('</i>'))
    )

    chsec_long = f'{chapter_id}: {chapter_title} - {section_no}: {section_heading}'
    chsec = f'Chapter: {chapter_no} | Section: {section_no}'
    return render_template(
        'gospel_page.html',
        chapter_no=chapter_no,
        chapter_title=chapter_title,
        section_heading=markupsafe_section_heading,
        section_text=markupsafe_section_text,
        chsec_long=chsec_long,
        chsec=chsec,
        **make_nav_links(chapter_no, section_no)
    )


def make_nav_links(chapter_no, section_no):
    chsecs = {
        'prev_section': get_previous_section_chsec(chapter_no, section_no),
        'next_section': get_next_section_chsec(chapter_no, section_no),
        'chapter_beginning': get_chapter_beginning_chsec(chapter_no),
        'chapter_end': get_chapter_end_chsec(chapter_no),
        'prev_chapter': get_previous_chapter_chsec(chapter_no),
        'next_chapter': get_next_chapter_chsec(chapter_no),
    }

    def make_url(chsec):
        chapter, section = chsec
        return url_for('gospel_page.read_gospel_section', chapter_no=chapter, section_no=section)

    urls = dict(
        (k, make_url(v)) for k, v in chsecs.items()
    )
    urls['random_section'] = url_for('gospel_page.read_random_section')

    return urls


@gospel_page_app.route('/random', methods=['GET'])
def read_random_section():
    chsec = get_chapter_no_section_no(get_random_section_index())
    chapter_no, section_no = chsec

    redirect_url = url_for('gospel_page.read_gospel_section', chapter_no=chapter_no, section_no=section_no)
    return redirect(redirect_url)

