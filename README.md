## Daily Ramana Mail

Google AppEngine app to send daily mail

## Run Locally
1. Clone this repo with

   ```
   git clone https://ramprax@bitbucket.org/ramprax/ramana_talk_mailer.git
   ```
2. Install dependencies in the project's lib directory.
   Note: App Engine can only import libraries from inside your project directory.

   ```
   cd ramana_talk_mailer
   pip install -r requirements.txt -t lib
   ```

Visit the application [http://ramana-talk-mailer.appspot.com](http://ramana-talk-mailer.appspot.com)

